package logica;

public class Celda {

	private char ValorCelda;
	private boolean celdaOcupada;

	Celda() {
		ValorCelda ='2';
		setCeldaOcupada(false);

	}

	protected void setValorCelda(char valorCelda) {
		if (valorCelda == 'X' || valorCelda == 'O') {

			ValorCelda = valorCelda;
			setCeldaOcupada(true);
		}else if (valorCelda == '2') {

			ValorCelda = valorCelda;
			setCeldaOcupada(false);
		}else
			throw new IllegalArgumentException("El char no es ni X ni O");
	}
	
	protected void vaciarCelda() {
		this.celdaOcupada=false;
		this.ValorCelda = '2';
	}

	// devuelve si la celda esta ocupada
	protected boolean isCeldaOcupada() {
		return celdaOcupada;
	}

	// setea el booleano celdaOcupada
	protected void setCeldaOcupada(boolean celdaOcupada) {
		this.celdaOcupada = celdaOcupada;
	}

	// devuelve el valor de la celda
	protected char getValorCelda() {
		return ValorCelda;
	}

}
