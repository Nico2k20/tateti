package logica;

public class VerificarTablero {

	protected static boolean verificarGanador(char caracter, Tablero tab) {

		return verificarVertical(caracter, tab) || verificarHorizontal(caracter, tab)
				|| verificarDiagonal(caracter, tab);
	}

	/////////////////////// Verificar///////////////////////////////////////
	/////////////////////// DIAGONALES///////////////////////////////////////
	private static boolean verificarDiagonal(char caracter, Tablero tab) {

		
		
		
		return diagonalIzquierda(caracter, tab) || diagonalDerecha(caracter, tab);

	}

	private static boolean diagonalDerecha(char caracter, Tablero tab) {
		tab.dibujarTablero();
		int contador =0;
		for (int i = tab.getTamanoTablero()-1; i>=0; i--) {
		
			if(tab.devolverCeldaEn(i, contador).getValorCelda()!=caracter)
					return false;
				contador++;
				
			
			
		}
		
		return true;
	}

	private static boolean diagonalIzquierda(char caracter, Tablero tab) {
		for (int i = 0; i < tab.getTamanoTablero(); i++) {
			
			if (tab.devolverCeldaEn(i, i).getValorCelda() != caracter&&caracter!='2')
				return false;

		}
		return true;
	}

	/////////////////////// Verificar Horizontal//////////////////////////////////

	private static boolean verificarHorizontal(char caracter, Tablero tab) {

		for (int i = 0; i < tab.getTamanoTablero(); i++) {
			boolean acumuladorBooleanoAnd = true;
			for (int j = 0; j < tab.getTamanoTablero(); j++) {

				Celda celda = tab.devolverCeldaEn(i, j);
				acumuladorBooleanoAnd = acumuladorBooleanoAnd && celda.getValorCelda() == caracter;
			}

			if (acumuladorBooleanoAnd)
				return true;

		}

		return false;

	}

	///////////////////// Verificar Vertical////////////////////////////////

	private static boolean verificarVertical(char caracter, Tablero tab) {

		for (int i = 0; i < tab.getTamanoTablero(); i++) {
			boolean acumuladorBooleanoAnd = true;
			for (int j = 0; j < tab.getTamanoTablero(); j++) {
				Celda celda = tab.devolverCeldaEn(j, i);
				acumuladorBooleanoAnd = acumuladorBooleanoAnd && celda.getValorCelda() == caracter;

			}
			if(acumuladorBooleanoAnd)
				return true;

		}

		return false;

	}
}
