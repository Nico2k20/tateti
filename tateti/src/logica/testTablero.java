package logica;

import static org.junit.Assert.*;

import org.junit.Test;

public class testTablero {

	// test creacion tablero
	@Test
	public void testCreacionCeldaTamano3by3() {
		Tablero tablero = new Tablero();

		assertTrue(tablero.getTamanoTablero() == 3);

	}

	// test tablero vacio
	@Test
	public void testTableroVacio() {

		Tablero tablero = new Tablero();

		assertTrue(tablero.estaVacio());
	}

	@Test
	public void testSetearCeldaX() {

		Tablero tablero = new Tablero();

		int indice1 = 1;
		int indice2 = 2;

		tablero.setearCelda(indice1, indice2, 'X');

		assertTrue(tablero.posOcupada(indice1, indice2));
	}

	@Test
	public void testSetearCeldaO() {

		Tablero tablero = new Tablero();

		int indice1 = 1;
		int indice2 = 2;

		tablero.setearCelda(indice1, indice2, 'O');

		assertTrue(tablero.posOcupada(indice1, indice2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetearCeldaArgIlegal() {

		Tablero tablero = new Tablero();

		int indice1 = 1;
		int indice2 = 2;

		tablero.setearCelda(indice1, indice2, 'l');

		assertTrue(tablero.posOcupada(indice1, indice2));
	}

	// setear celda Argumentos ilegales
	@Test(expected = IllegalArgumentException.class)
	public void testSetearCeldaArgumentosIlegales() {

		Tablero tablero = new Tablero();

		int indice1 = 3;
		int indice2 = 3;

		tablero.setearCelda(indice1, indice2, 'O');

	}

	//////////////////////// --------Tests Verificacion Tablero
	//////////////////////// ---///////////////////

	// Ganador diagonal
	@Test
	public void testGanadorDiagonalIZQ() {
		Tablero tab = new Tablero();

		diagonalGanadorIZQX(tab);

		assertTrue(VerificarTablero.verificarGanador('X', tab));

	}

	@Test
	public void testNoHayGanadorDiagonalIZQ() {
		Tablero tab = new Tablero();

		diagonalNoHayGanadorIZQX(tab);

		assertFalse(VerificarTablero.verificarGanador('X', tab));

	}

	@Test
	public void testGanadorDiagonalDER() {
		Tablero tab = new Tablero();

		diagonalGanadorDERX(tab);

		assertTrue(VerificarTablero.verificarGanador('X', tab));

	}
	@Test
	public void testGanadorDiagonalDERO() {
		Tablero tab = new Tablero();

		diagonalGanadorDERO(tab);

		assertTrue(VerificarTablero.verificarGanador('O', tab));

	}

	@Test
	public void testNoHayGanadorDiagonalDER() {
		Tablero tab = new Tablero();

		diagonalNoHayGanadorDERX(tab);

		assertFalse(VerificarTablero.verificarGanador('X', tab));

	}

	@Test
	public void testNoHayGanadorHorizontal() {
		Tablero tab = new Tablero();

		horizontalNoGanadorX(tab);

		assertFalse(VerificarTablero.verificarGanador('X', tab));

	}

	@Test
	public void testGanadorHorizontal() {
		Tablero tab = new Tablero();

		horizontalGanadorX(tab);

		assertTrue(VerificarTablero.verificarGanador('X', tab));

	}

	@Test
	public void testGanadorVertical() {
		Tablero tab = new Tablero();

		ganadorVertical(tab);

		assertTrue(VerificarTablero.verificarGanador('X', tab));

	}

	@Test
	public void testNoHayGanadorVertical() {
		Tablero tab = new Tablero();

		noHayganadorVertical(tab);

		assertFalse(VerificarTablero.verificarGanador('X', tab));
		
		

	}

	private void noHayganadorVertical(Tablero tab) {
		tab.setearCelda(0, 0, 'X');
		tab.setearCelda(1, 0, 'X');

		/*
		 * 		|X |''|''|
		 * 		|X |''|''|
		 * 		|''|''|''|
		 * */
	}

	private void ganadorVertical(Tablero tab) {

		tab.setearCelda(0, 0, 'X');
		tab.setearCelda(1, 0, 'X');
		tab.setearCelda(2, 0, 'X');

		/*
		 * 		|X|''|''|
		 * 		|X|''|''|
		 * 		|X|''|''|
		 * */
	}

	private void horizontalGanadorX(Tablero tab) {
		tab.setearCelda(0, 0, 'X');
		tab.setearCelda(0, 1, 'X');
		tab.setearCelda(0, 2, 'X');
	
		/*
		 * 		|X |X |X |
		 * 		|''|''|''|
		 * 		|''|''|''|
		 * */
	}

	private void diagonalGanadorIZQX(Tablero tablero) {

		tablero.setearCelda(0, 0, 'X');
		tablero.setearCelda(1, 1, 'X');
		tablero.setearCelda(2, 2, 'X');
	
		/*
		 * 		|X | '' |''|
		 * 		|''| X  |''|
		 * 		|''|''  |X |
		 * */
	}

	private void diagonalGanadorDERX(Tablero tablero) {

		tablero.setearCelda(2, 0, 'X');
		tablero.setearCelda(1, 1, 'X');
		tablero.setearCelda(0, 2, 'X');
	
		/*
		 * 		|''|''|X|
		 * 		|''|X|''|
		 * 		|X|''|''|
		 * */
	
	}
	private void diagonalGanadorDERO(Tablero tablero) {

		tablero.setearCelda(2, 0, 'O');
		tablero.setearCelda(1, 1, 'O');
		tablero.setearCelda(0, 2, 'O');
	
		/*
		 * 		|''|''|O|
		 * 		|''|O|''|
		 * 		|O|''|''|
		 * */
	
	}
	

	private void diagonalNoHayGanadorDERX(Tablero tab) {
		tab.setearCelda(2, 2, 'X');
		tab.setearCelda(1, 1, 'X');
	
		/*
		 * 		|''|''|X |
		 * 		|''|X |''|
		 * 		|''|''|''|
		 * */
		
	}

	private void horizontalNoGanadorX(Tablero tab) {
		tab.setearCelda(0, 1, 'X');
		tab.setearCelda(0, 2, 'X');

		/*
		 * 		|X |X |''|
		 * 		|''|''|''|
		 * 		|''|''|''|
		 * */
	
	}

	private void diagonalNoHayGanadorIZQX(Tablero tab) {

		tab.setearCelda(0, 0, 'X');
		tab.setearCelda(0, 1, 'X');

		/*
		 * 		|X | X |''|
		 * 		|''|''|''|
		 * 		|''|''|''|
		 * */
	}

}
