package logica;

public class Tablero {

	private Celda[][] tablero;
	private final int tamanoTablero = 3;

	public Tablero() {

		this.tablero = new Celda[tamanoTablero][tamanoTablero];

		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				Celda celda = new Celda();
				tablero[i][j] = celda;

			}

		}

	}

	// devuelve el tamano del tablero
	protected int getTamanoTablero() {
		return tablero[0].length;
	}

	protected Celda devolverCeldaEn(int pos1, int pos2) {

		return tablero[pos1][pos2];
	}

	// devuelve si esta vacio o no vacio el tablero
	protected boolean estaVacio() {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				if (this.tablero[i][j].isCeldaOcupada())
					return false;

			}

		}

		return true;
	}

	public void reiniciarCeldas() {

		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				tablero[i][j].vaciarCelda();

			}

		}

	}
	public void dibujarTablero() {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				System.out.print("|"+tablero[i][j].getValorCelda()+"|");
				
			}
			System.out.println("");
		}
		
	}
	

	public boolean esEmpate() {
//		 dibujarTablero();
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				if (this.tablero[i][j].isCeldaOcupada()==false)
					return false;

			}
		}
		return true;
	}

	// setea el valor de una celda dados los argumentos
	public void setearCelda(int indice1, int indice2, char caracter) {
		if (indice1 >= tamanoTablero || indice2 >= tamanoTablero)
			throw new IllegalArgumentException("Indices ilegales");

		this.tablero[indice1][indice2].setValorCelda(caracter);

	}

	// devuel si la posicion de los argumentos esta ocupada
	protected boolean posOcupada(int indice1, int indice2) {
		if (indice1 >= tamanoTablero || indice2 >= tamanoTablero)
			throw new IllegalArgumentException("Indices ilegales");

		return this.tablero[indice1][indice2].isCeldaOcupada();

	}

	public boolean ganador(char caracter) {

		return VerificarTablero.verificarGanador(caracter, this);

	}

}
