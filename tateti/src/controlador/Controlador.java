package controlador;

import logica.Tablero;

public class Controlador {

	Tablero tab;

	public Controlador() {
		this.tab = new Tablero();
	}

	public int  existeGanador() {
		
//		tab.dibujarTablero();
		
		if (this.tab.ganador('X'))
			return 1;
		else if (this.tab.ganador('O'))
			return 0;

		
		
		return -1;
	}

	public boolean esEmpate() {
		
		return this.tab.esEmpate();
		
	}
	
	public void recrearTablero(int [][] tablero) {
		
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				if(tablero[i][j]==1) 
					setearTablero('X',i,j);
				else if (tablero[i][j]==0)
					setearTablero('O',i,j);
				else
					setearTablero('2',i,j);
			}
		}
		
		
	}

		
	public void vaciarTablero() {
		
		this.tab.reiniciarCeldas();
	}
	
	
	/*Setar posiciones del tablero*/
	private void setearTablero(char caracter, int pos1, int pos2) {

		try {

			this.tab.setearCelda(pos1, pos2, caracter);
		} 
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
