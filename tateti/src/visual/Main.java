package visual;

import controlador.Controlador;

public class Main {

	public static void main(String[] args) {

		Controlador cont = new Controlador();
		InterfazVisual gui = new InterfazVisual();
		ListenerMouse listener = new ListenerMouse(gui,cont);
		gui.addMouseListener(listener);
	}

}
