package visual;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;

import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.BorderLayout;

public class InterfazVisual {

	// ventana
	private JFrame frame;
	// atributos de panel
	private JPanel panelTablero, panelLabel;
	private final int tamanoTablero = 3;
	private GridLayout layoutTablero;
	private ArrayList<ArrayList<JTextField>> celdas;
	private int[][] celdasSeleccionadas;
	private final Font font = new Font("Arial", Font.BOLD, 40);
	private final Font fontLabel = new Font("Arial", Font.BOLD, 15);
	private boolean turnoX;
	private final char X = 'X';
	private final char O = 'O';
	private final Color colorX = Color.blue;
	private final Color colorO = Color.red;
	private final Color colorPanel = Color.ORANGE;
	private JLabel labelTurnoGanador;
	private JPanel panel;
	private JTextField contadorAzul;
	private JTextField contadorRojo;
	private int contadorPuntosAzul;
	private int contadorPuntosRojo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazVisual window = new InterfazVisual();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfazVisual() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(colorPanel);

		// inicializado panel Tablero
		this.panelTablero = new JPanel();
		this.panelTablero.setBounds(113, 69, 263, 189);
		this.frame.getContentPane().add(panelTablero);

		panelLabel = new JPanel();
		panelLabel.setBounds(113, 27, 105, 40);
		panelLabel.setBackground(colorO);
		panelLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
		frame.getContentPane().add(panelLabel);

		labelTurnoGanador = new JLabel("Turno O");
		
		panelLabel.add(labelTurnoGanador);

		panel = new JPanel();
		panel.setBounds(236, 27, 62, 40);
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(1, 0, 0, 0));

		/* contador azul */
		contadorAzul = new JTextField();
		panel.add(contadorAzul);
		contadorAzul.setColumns(10);

		contadorAzul.setBackground(colorX);
		contadorAzul.setEditable(false);
		contadorAzul.setText("0");
		this.contadorPuntosAzul = 0;
		this.contadorAzul.setHorizontalAlignment(SwingConstants.CENTER);
		contadorAzul.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

		/* contador rojo */
		contadorRojo = new JTextField();
		panel.add(contadorRojo);
		contadorRojo.setColumns(10);
		contadorRojo.setBackground(colorO);
		contadorRojo.setEditable(false);
		contadorRojo.setText("0");
		this.contadorPuntosRojo = 0;
		
		contadorRojo.setHorizontalAlignment(SwingConstants.CENTER);
		contadorRojo.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

		this.celdas = new ArrayList<ArrayList<JTextField>>();

		// Crea la cuadricula 3*3 en el panel
		crearCuadricula();

		this.frame.setResizable(false);
		this.frame.setVisible(true);

	}

	public int[][] devolverCeldasSeleccionada() {
		celdasSeleccionadas = new int[this.tamanoTablero][this.tamanoTablero];

		for (int i = 0; i < this.tamanoTablero; i++) {
			for (int j = 0; j < this.tamanoTablero; j++) {

				if (this.celdas.get(i).get(j).getText().length() > 0) {
					if (this.celdas.get(i).get(j).getText().charAt(0) == 'X')
						celdasSeleccionadas[i][j] = 1;
					else if (this.celdas.get(i).get(j).getText().charAt(0) == 'O')
						celdasSeleccionadas[i][j] = 0;
				} else
					celdasSeleccionadas[i][j] = -1;

			}

		}

		return celdasSeleccionadas;

	}

	private void crearCuadricula() {

		this.layoutTablero = new GridLayout(this.tamanoTablero, this.tamanoTablero);

		// seteado layout
		this.panelTablero.setLayout(this.layoutTablero);

		for (int i = 0; i < this.tamanoTablero; i++) {
			ArrayList<JTextField> fila = new ArrayList<JTextField>();

			for (int j = 0; j < this.tamanoTablero; j++) {
				// crear la celda
				JTextField nuevaCelda = new JTextField();
				nuevaCelda.setEditable(false);

				// Setea el grosor de los lados del JtextField
				nuevaCelda.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

				// agregar al panel tablero
				this.panelTablero.add(nuevaCelda);
				fila.add(nuevaCelda);

			}
			// agregar la fila
			this.celdas.add(fila);

		}
		frame.setBackground(colorPanel);
	}

	public void addMouseListener(MouseListener listener) {

		this.panelTablero.addMouseListener(listener);
		for (ArrayList<JTextField> fila : this.celdas) {
			for (JTextField celda : fila) {
				celda.addMouseListener(listener);

			}
		}
	}

	/* Dibujar Celdas */
	public void dibujarJText() {

		if (turnoX == true) {
			this.turnoX = false;
			this.setearCeldaSeleccionada(this.X);
		} else {
			this.turnoX = true;
			this.setearCeldaSeleccionada(this.O);

		}

	}

	public void dibujarLabel() {
		this.labelTurnoGanador.setFont(fontLabel);
		if (turnoX == true) {
			this.panelLabel.setBackground(colorX);

			this.labelTurnoGanador.setText("Turno de X");
		} else {
			this.panelLabel.setBackground(colorO);
			this.labelTurnoGanador.setText("Turno de O");

		}
	}

	public void dibujarLabelGanador(int enteroGanador) {

		if (enteroGanador == -1)
			return;
		else if (enteroGanador == 1) {
			this.contadorPuntosAzul++;
			dibujarContador(enteroGanador);
			this.panelLabel.setBackground(colorX);
			this.labelTurnoGanador.setText("Gano X");
		} else {
			this.contadorPuntosRojo++;
			dibujarContador(enteroGanador);
			this.panelLabel.setBackground(colorO);
			this.labelTurnoGanador.setText("Gano O");
		}

	}

	public void dibujarContador(int enteroGanador) {

		if (enteroGanador == 1) {
			this.contadorAzul.setText(this.contadorPuntosAzul + "");

		}

		if (enteroGanador == 0) {
			this.contadorRojo.setText(this.contadorPuntosRojo + "");
		}

	}

	public void reiniciarTablero() {

		this.celdasSeleccionadas = new int[this.tamanoTablero][this.tamanoTablero];

		for (int i = 0; i < this.celdas.size(); i++) {
			for (int j = 0; j < this.celdas.size(); j++) {
				this.celdas.get(i).get(j).setText("");
				this.celdas.get(i).get(j).setBackground(Color.WHITE);

			}

		}
	}

	public JTextField escucharCeldas() {

		for (ArrayList<JTextField> celda : celdas) {
			for (JTextField text : celda) {
				if (text.isFocusOwner() && text.getText().isEmpty())
					return text;
			}
		}

		return null;
	}

	public void setearCeldaSeleccionada(char caracter) {
		JTextField text = escucharCeldas();
		if (text == null)
			return;

		if (caracter == 'O')
			text.setBackground(this.colorO);
		if (caracter == 'X')
			text.setBackground(this.colorX);

		text.setFont(this.font);
		text.setAlignmentX(text.getAlignmentX() / 2);
		text.setAlignmentY(text.getAlignmentY() / 2);
		text.setText(caracter + "");
		text.setHorizontalAlignment(SwingConstants.CENTER);

	}
}
