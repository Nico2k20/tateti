package visual;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import controlador.Controlador;

public class ListenerMouse implements MouseListener {

	InterfazVisual gui;
	Controlador controlador;
	ListenerMouse(InterfazVisual gui, Controlador controlador ) {
		this.gui = gui;
		this.controlador=controlador;

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		 this.gui.dibujarJText();
		 this.gui.dibujarLabel();
		 /*Posiciones seleccionadas en la parte visual*/
		 int [][] tablero = (this.gui.devolverCeldasSeleccionada());
		 
		
		 
		 
		 this.controlador.recrearTablero(tablero);
		 int existeGanador =this.controlador.existeGanador();
		
		 System.out.println(existeGanador+"existe ?");
		 
		 if (existeGanador!=-1) {
			 this.gui.dibujarLabelGanador(existeGanador);
			 this.gui.reiniciarTablero();
			 this.controlador.vaciarTablero();
		 }
		 System.out.println(this.controlador.esEmpate());
		 if(this.controlador.esEmpate()) {
			 this.gui.reiniciarTablero();
			 this.controlador.vaciarTablero();
		 }
				
			
		 
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
